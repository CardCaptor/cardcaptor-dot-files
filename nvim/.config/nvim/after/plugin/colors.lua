function ColorMyPencils(color)
	color = color or "tokyonight-storm"
	vim.cmd.colorscheme(color)

	vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
	vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end

ColorMyPencils()

function PimpMyLualine(theme)
    theme = theme or "auto"
    local lualine = require('lualine')

    lualine.setup ({
        options = {
            icons_enabled = true,
            theme = theme,
        }
    })
end

PimpMyLualine()

