local builtin = require('telescope.builtin')
vim.g.mapleader = " "
vim.keymap.set('n', '<C-p>', function ()
    builtin.find_files({path_display={"shorten"}});
end)
vim.keymap.set('n', '<S-p>', function()
    builtin.git_files({path_display={"shorten"}});
end)
vim.keymap.set('n', '<C-f>', function()
	builtin.grep_string({path_display={"shorten"}, search = vim.fn.input("Grep > ")});
end)
