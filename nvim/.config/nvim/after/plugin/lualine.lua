local icons = require('nvim-web-devicons')
local lualine = require('lualine')

icons.setup()

lualine.setup ({
    options = {
        icons_enabled = true,
        theme = 'auto',
    }
})
