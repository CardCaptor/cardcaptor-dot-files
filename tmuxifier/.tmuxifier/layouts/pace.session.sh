# Set a custom session root path. Default is `$HOME`.
# Must be called before `initialize_session`.
session_root "~/repos/insite-ai/customers/reynolds/rn-pace"

# Create session with specified name if it does not already exist. If no
# argument is given, session name will be based on layout file name.
if initialize_session "insite-pace"; then

  # Create a new window inline within session layout definition.
  new_window "terminal"
  select_pane 0
  run_cmd "source .env"
  run_cmd "nvm use"
  run_cmd "cd apps/pace"
  run_cmd "aws sso login"
  new_window "editor"
  select_pane 0
  run_cmd "cd apps/pace"
  run_cmd "nvim ."

fi

# Finalize session creation and switch/attach to it.
finalize_and_go_to_session
