# Set a custom session root path. Default is `$HOME`.
# Must be called before `initialize_session`.
session_root "~/repos/"

# Create session with specified name if it does not already exist. If no
# argument is given, session name will be based on layout file name.
if initialize_session "ha"; then

  # Create a new window inline within session layout definition.
  new_window "api editor"
  select_pane 0
  run_cmd "cd home-accounting-api"
  run_cmd "nvim ."
  new_window "ui editor"
  select_pane 0
  run_cmd "cd home-accounting-ui"
  run_cmd "nvim ."
  new_window "api terminal"
  select_pane 0
  run_cmd "cd home-accounting-api"
  run_cmd "bun dev"
  new_window "ui terminal"
  select_pane 0
  run_cmd "cd home-accounting-ui"
  run_cmd "bun dev"

fi

# Finalize session creation and switch/attach to it.
finalize_and_go_to_session
